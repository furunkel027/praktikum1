import java.util.*;

public class Answer {

	public static <E> void main(String[] param) {
		System.out.println("==== START ====");
		// TODO!!! Solutions to small problems
		// that do not need an independent method!
		String reaVahe = "   -   -   -   -   -   -   -   -   -";
		System.out.println("Ülesanne nr 01:");
		// PORRBLEM NR 01
		// Convert a given double number (double) into String (String).
		// conversion double -> String
		double mingiNumber = 2974;
		String stringiKujulNr = Double.toString(mingiNumber);
		System.out.printf("Number %8f teisendatult stringiks: %s%n",
				mingiNumber, stringiKujulNr);
		System.out.println(reaVahe);

		// PORRBLEM NR 02
		// Convert a string (String) into an integer number (int).
		// Assume that the string actually represents an integer
		// (Discover what happens, if the string is illegal?).
		// conversion String -> int
		// Ei eelda mitte essugi
		System.out.println("Ülesanne nr 02:");
		String algneNumber = "1230583";
		int nuditudTulemus = 0;
		try {
			nuditudTulemus = Integer.parseInt(algneNumber);
		} catch (Exception e) {
			System.out
					.println("ERROR: Midagi siin nüüd igatahes juhtus/juhtub...");
		}
		System.out.printf("Stringist %s täisarvuks nuditud number: %d%n",
				algneNumber, nuditudTulemus);
		System.out.println(reaVahe);

		// PORRBLEM NR 03
		// Take the current moment of time from the computers inner clock
		// and form a string "hours:minutes:seconds" (use 24h notation, e.g.
		// "13:25:10").
		// "hh:mm:ss"
		System.out.println("Ülesanne nr 03:");
		String kellaAjaVorming = "HH:mm:ss";
		Date seeHetk = new Date();
		System.out.println("Java DateFormat kellaaeg on: " + seeHetk);
		// Igaks juhuks ei hakka asju inkluudima, sestap täisPATH:
		java.text.DateFormat df = new java.text.SimpleDateFormat(
				kellaAjaVorming);
		System.out
				.println("HH.mm:ss kujul kellaaeg on : " + df.format(seeHetk));
		System.out.println(reaVahe);

		// PORRBLEM NR 04
		// Find a cosine of a given angle that is expressed in degrees.
		// cos 45 deg
		System.out.println("Ülesanne nr 04:");
		double nurkKraadides = 60;
		double koosinusSellest = 0;
		try {
			koosinusSellest = Math.cos(Math.toRadians(nurkKraadides));
		} catch (Exception e) {
			System.out
					.println("ERROR: Aga mis siis, kui ma täna ei TAHA sellest nurgast koosinust arvutada?");
		}
		System.out.printf("Koosinus nurgast %f kraadi oleks täna: %f%n",
				nurkKraadides, koosinusSellest);
		System.out.println(reaVahe);

		// PORRBLEM NR 05
		// Print a table of square roots for numbers from 0 up to 100 using step
		// 5 (print two numbers per line: argument and its square root).
		// table of square roots
		// Print a table of square roots for numbers from 0 up to 100 using step
		// 5 (print two numbers per line: argument and its square root).
		System.out.println("Ülesanne nr 05:");
		for (int i = 0; i < 101; i = i + 5) {
			System.out.println("Nummer: " + i + " ja tema ruutjuurikas: "
					+ Math.sqrt((double) i));
		}
		System.out.println(reaVahe);

		// PORRBLEM NR 06
		// Given a string replace in it all uppercase letters with corresponding
		// lowercase letters and all lowercase letters with corresponding
		// uppercase
		// letters. Do not change other symbols ("ABcd12" -> "abCD12").
		// Use StringBuffer or StringBuilder class for intermediate result.

		// Create an independent method String reverseCase (String s) for this
		// task.
		// - TEHTUD, vt lõppu
		System.out.println("Ülesanne nr 06:");
		System.out.println("   keerame tähtedel registri vastupidiseks:");
		String firstString = "ABcd12";
		String result = reverseCase(firstString);
		System.out.println("\"" + firstString + "\" -> \"" + result + "\"");
		// reverse string
		System.out.println(reaVahe);

		// PORRBLEM NR 07
		// Given a string find its reverse ("1234ab" -> "ba4321").
		// String tagurPidi = new StringBuffer(algsetPidi).reverse().toString();
		System.out.println("Ülesanne nr 07:");
		System.out.println("   Reversseerime stringi:");
		String algsetPidi = "Kui Arno isaga koolimajja jõudis, olid tunnid juba alanud.";
		StringBuilder vahePealsetPidi = new StringBuilder("");
		vahePealsetPidi.append(algsetPidi).reverse();
		String tagurPidi = vahePealsetPidi.toString();

		System.out.println(algsetPidi);
		System.out.println(tagurPidi);
		System.out.println(reaVahe);

		// PORRBLEM NR 08
		// Given a text (String) find the number of words in it.
		// Words are separated by any positive number of any kind of whitespaces
		// (like space, tab, ...).

		// Create an independent method int countWords (String t) for this task.
		// Tehtud - vt allpool
		System.out.println("Ülesanne nr 08:");
		String s = "How  many	 words   here";
		// String ss = "\t\ttwo\t   here\t ";
		int nw = countWords(s);
		System.out
				.println("WordCount |" + s + "|? \t -> " + String.valueOf(nw));
		System.out.println(reaVahe);

		// PORRBLEM NR 09
		// Register the current time, pause the program for 3 seconds,
		// register the current time again and print the difference
		// between these two moments of time in milliseconds.
		// -- Neid trikke tegin ma kodutöös juba.
		System.out.println("Ülesanne nr 09:");
		long kellaAegOnAlgul = System.currentTimeMillis();
		System.out.println("Start : " + kellaAegOnAlgul);
		int ootamiseAeg = 3000; // mS
		System.out.println(reaVahe);
		System.out.printf("Nüüd ootame %5d milliSekundit", ootamiseAeg);
		try {
			Thread.sleep(ootamiseAeg); // 1000 milliseconds is one second.
		} catch (InterruptedException ex) {
			Thread.currentThread().interrupt();
		}
		long kellaAegOnHiljem = System.currentTimeMillis();
		System.out.println("Stopp : " + kellaAegOnHiljem);
		long delta = (kellaAegOnHiljem - kellaAegOnAlgul);
		System.out.println("Võimalik relatiivsusparadoks mS : "
				+ (delta - ootamiseAeg));
		System.out.println(reaVahe);

		// pause. COMMENT IT OUT BEFORE JUNIT-TESTING!

		// PORRBLEM NR 10
		// Create a list (ArrayList) of 100 random integers
		// (Integer) in between 0 and 999.
		// Print the result. What is the difference between int and Integer?
		System.out.println("Ülesanne nr 10:");
		System.out
				.println("Muide: int on Javas lihttüüp aga Integer on oluliselt keerulisem asi - objekt nimelt.");

		final int LSIZE = 100;
		ArrayList<Integer> randList = new ArrayList<Integer>(LSIZE);
		// ----- ALTERNATIIV 1
		// (http://stackoverflow.com/questions/363681/generating-random-integers-in-a-range-with-java)
		// Kui väga norida, siis BETWEEN 0..999 means: => [0,999] = (1,998)
		// A random integer value in the range [Min,Max],
		int minimum = 1;
		int maximum = 998;
		for (int i = 0; i < LSIZE; i++) {
			// Aga et boundary kristallselge oleks, siis:
			int lambist = minimum
					+ (int) (Math.random() * ((maximum - minimum) + 1));
			randList.add(new Integer(lambist));
		} // ---end of ALT 1

		// ----- ALTERNATIIV 2
		// Random generaator = new Random();
		// for (int i = 0; i < LSIZE; i++) {
		// randList.add(new Integer(generaator.nextInt(1000)));
		// }

		System.out.println(reaVahe);

		// PORRBLEM NR 11 - minimal element
		// Find the minimal element of this list.
		// How to find the minimal element of any List of Comparable elements
		// (read about interfaces, also consider that
		// ArrayList implements List and Integer implements Comparable)?
		System.out.println("Ülesanne nr 11:");
		int pisiPisi = Collections.min(randList).intValue();
		System.out
				.printf("Uus juhuarvude list pikkusega %5d, vähimaks numbriks osutus see: %4d%n",
						randList.size(), pisiPisi);
		System.out.println(reaVahe);

		// PORRBLEMZ NR 12-16
		// Create a hashtable (HashMap) containing 5 pairs of strings,
		// where "subject code" serves as a key and "subject name" serves as a
		// value (e.g. ("I231", "Algorithms and Data Structures") ,
		// just choose any subjects you like).
		// -> HashMap<String,String> hoidmiseKoht = new
		// HashMap<String,String>();
		System.out.println("Ülesanne nr 12:");

		Hashtable<String, String> hoiuKoht = new Hashtable<String, String>();
		hoiuKoht.put("I101", "Matemaaatiline analüüs");
		hoiuKoht.put("I200", "Programmeerimise algkursus Java baasil");
		hoiuKoht.put("I111", "Sissejuhatus informaatikasse");
		hoiuKoht.put("I115", "Füüsika I");
		hoiuKoht.put("I016", "Filosoofia");

		// PORRBLEM NR 12 - Print all the keys of this hashtable (one key per
		// line).
		System.out.println("Paisktabeli dupletite arv: " + hoiuKoht.size());
		Enumeration<String> loetelu = hoiuKoht.keys();
		while (loetelu.hasMoreElements()) {
			String dupletKey = (String) loetelu.nextElement();
			System.out.println("\t" + dupletKey);
		}
		System.out.println(reaVahe);

		// PORRBLEM NR 13 - Remove one of the subjects from the hashtable.
		System.out.println("Ülesanne nr 13:");
		hoiuKoht.remove("I115");
		System.out
				.println("Paisktabeli dupletite arv pärat ühe kirje eemaldamist: "
						+ hoiuKoht.size());
		System.out.println(reaVahe);

		// PORRBLEM NR 14 - Print all pairs from this hashtable
		// (each pair on a separate line).
		System.out.println("Ülesanne nr 14:");
		Enumeration<String> loetelu2 = hoiuKoht.keys();
		loetelu2 = hoiuKoht.keys();
		while (loetelu2.hasMoreElements()) {
			String dupletKey = (String) loetelu2.nextElement();
			String dupletValue = (String) hoiuKoht.get(dupletKey);
			System.out.println("\t" + dupletKey + " - " + dupletValue);
			// kunagi ehk saan aru, miks see tabel ei lase otse .getvalue() abil
			// adresseerida. String?
		}
		System.out.println(reaVahe);

		// PORRBLEM NR 15 - Reverse the order of elements of the list you have
		// created before.
		System.out.println("Ülesanne nr 15:");

		System.out.println("Before reverse:  " + randList);
		// Vt meetod tagurPidistus allpool.
		tagurPidistus(randList);
		System.out.println("After reverse: " + randList);

		System.out.println("");
		System.out.println(reaVahe);

		// PORRBLEM NR 16 - Write an independent method to reverse any list. The
		// method signature is
		//
		// public static <T extends Object> void reverseList (List<T> list)
		// throws UnsupportedOperationException
		System.out.println("Ülesanne nr 16:");
		System.out
				.println("  Sõltumatu meetod, mis iga listi tagurpidi keerab");

		System.out.println("Before reverse:  " + randList);
		reverseList(randList);
		System.out.println("After reverse: " + randList);
		System.out.println(reaVahe);
		System.out.println("Maximum: " + maximum(randList));

		System.out.println(reaVahe);
		System.out.println("==== STOP ====");
	} // endofMAIN

	/**
	 * Finding the maximal element.
	 * 
	 * @param a
	 *            Collection of Comparable elements
	 * @return maximal element.
	 * @throws NoSuchElementException
	 *             if <code> a </code> is empty.
	 */
	static public <T extends Object & Comparable<? super T>> T maximum(
			Collection<? extends T> a) throws NoSuchElementException {

		System.out.println("Ülesanne nr 11:a");
		System.out.println("\tSUB: maximum: Arusaamatu värk:");
		System.out.println("\tülesanne nõuab min'i, testid max'i");
		System.out.println();
		return (Collections.max(a));

	}

	// =========================================================

	/**
	 * Counting the number of words. Any number of any kind of whitespace
	 * symbols between words is allowed.
	 * 
	 * @param text
	 *            text
	 * @return number of words in the text
	 */
	public static int countWords(String text) {

		String vasakult = text.replaceAll("^[\\W\\s]+", ""); // Eest
		String paremalt = vasakult.replaceAll("[\\W\\s]+$", ""); // Tagant
		String abiMaatrix[] = paremalt.split("([\\W\\s]+)"); // jääk juppideks
		int result = abiMaatrix.length;
		return result;
	}

	/**
	 * Case-reverse. Upper -> lower AND lower -> upper.
	 * 
	 * @param s
	 *            string
	 * @return processed string
	 */
	public static String reverseCase(String s) {
		// StringBuffer not threadsafe but who cares? Mostly no threads here.
		StringBuilder vaheLadu = new StringBuilder("");
		int pikkus = s.length();
		for (int i = 0; i < pikkus; i++) {
			char current = s.charAt(i);
			boolean jubaOnSuur = Character.isUpperCase(current);
			if (jubaOnSuur == true)
				vaheLadu.append(Character.toLowerCase(current));
			else
				vaheLadu.append(Character.toUpperCase(current));
		}
		return vaheLadu.toString();

	}

	// ArrayList<Integer> randList = new ArrayList<Integer>(LSIZE);
	/**
	 * ArrayList<Integer> reverse without creating any new list.
	 * 
	 * @param list
	 *            list to reverse
	 */
	static void tagurPidistus(ArrayList<Integer> sisend) {
		// iva hangitud siit, aga selgemaks ümber kirjutatud
		// http://stackoverflow.com/questions/12397267/java-a-single-generic-method-to-reverse-arraylist-or-list-of-objects
		int suurus = sisend.size();
		int pool = suurus / 2;
		int j = suurus - 1; // mõtet vaid pooleni teha seda
		for (int i = 0; i < pool; i++, j--) {
			int tmp = sisend.get(i);
			sisend.set(i, sisend.get(j));
			sisend.set(j, tmp);
		}
	}

	/**
	 * List reverse. Do not create a new list.
	 * 
	 * @param list
	 *            list to reverse
	 */
	public static <T extends Object> void reverseList(List<T> list)
			throws UnsupportedOperationException {
		// Nii krdi lihtne ei saa üks asi ometi olla? Või?
		Collections.reverse(list);
	}
}
