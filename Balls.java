/**
 * The class is destined for the homework assignment v 1.1 - The skeleton
 * attributed to jpoial. Src https://jpoial@bitbucket.org/i231/praktikum1.git
 * TRYING with the QUICKsort (without the recursions and final swap.
 */
public class Balls {

	/**
	 * Only two values - green and red. NB! Sorting ordinarity is kept
	 * elsewhere: 1. -RED, 2. green
	 */
	enum Color {
		green, red
	};

	// Teooriat vahtida siit:
	// Born in Moscow - http://en.wikipedia.org/wiki/Quicksort
	// Pseudokood oli asjalik ka siin:
	// http://www.myassignmenthelp.net/quicksort-assignment-help.php

	// MAIN only needed for debugging and array generation
	public static void main(String[] param) {

	} // END of MAIN

	// swap
	public static void swapTwoBalls(Balls.Color matrix[], int seller, int buyer) {
		Color tempStorage = matrix[seller];
		matrix[seller] = matrix[buyer];
		matrix[buyer] = tempStorage;
		return;
	}

	public static int partition(Balls.Color[] matrix, int lo, int hi) {
		int size = (hi - lo);
		if (size < 2)
			return 0;

		Color pivotValue = Balls.Color.green; // For two-elements set.
		int movingPointer = lo;
		for (int i = lo; (i < (hi + 1)); i++) { //
			boolean ballIsRed = (!matrix[i].equals(pivotValue));
			if (ballIsRed) { // Reds go to the left side
				swapTwoBalls(matrix, i, movingPointer);
				movingPointer++;
			}
		}
		return movingPointer;
	}

	public static void quickSort(Balls.Color[] sorditav, int algus, int ots) {

		int median = partition(sorditav, algus, ots);
		if (median < 0)
			return;
	}

	// Wrapper method to satisfy the automate tests
	public static void reorder(Color[] balls) {
		// Special cases
		if (balls.length < 2)
			return;
		int left = 0;
		int right = (balls.length - 1); // parem äär, java indexing
		quickSort(balls, left, right);

	} // end of REORDER
} // end of MAIN
